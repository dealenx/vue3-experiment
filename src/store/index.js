import { createStore } from "vuex";

export const store = createStore({
  state() {
    return {
      todos: [
        {
          done: false,
          title: "Test Title",
        },
      ],
    };
  },
  actions: {
    add: ({ commit }, payload) => {
      commit("ADD", payload);

      return payload;
    },
    delete: ({ commit, dispatch, state }, payload) => {
      commit("DELETE", payload.id);
    },
  },
  mutations: {
    ADD: (state, payload) => {
      state.todos.push({
        ...payload,
      });
    },
    DELETE: (state, id) => {
      const index = state.todos.findIndex((item) => item.id == id);
      state.todos.splice(index, 1);
    },
  },
});
