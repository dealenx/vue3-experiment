import { ref, computed } from "vue";
import { useStore } from "vuex";

export default function useTodos() {
  const store = useStore();

  const newTodo = ref("");
  const todos = computed(() => store.state.todos);

  const addTodo = () => {
    const value = newTodo.value && newTodo.value.trim();
    if (!value) {
      return;
    }

    const payload = {
      title: value,
      done: false,
    };

    store.dispatch("add", payload);

    newTodo.value = "";
  };

  const removeTodo = (payload) => {
    // const index = items.value.findIndex((item) => item === payload);
    // items.value.splice(index, 1);
    store.dispatch("delete", payload);
  };

  return {
    addTodo,
    newTodo,
    removeTodo,
    todos,
  };
}
